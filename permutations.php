<?php
//Spencer Weiss
//Precalculus 4/5
//November 3, 2010
function permute($a) //Finds the next lexicographic permutation of an array
{
	$k = -1;
	foreach($a as $index => $value)
	{
		if(isset($a[$index+1]) && $a[$index]<$a[$index+1])
		{
			$k = $index; //the highest index such that $a[$k] < $a[$k+1]
		}
		if($a[$k] < $a[$index])
		{
			$l = $index; //The highest index such that $a[$k] < $a[$l];
		}
	}
	if($k == -1)
	{
		return false;
	}
	$n = $a[$l]; //Swap values
	$a[$l] = $a[$k];
	$a[$k] = $n;
	$permutation = array_merge(array_slice($a,0,$k+1), array_reverse(array_slice($a,$k+1))); //reverse array after index $k+1
	return $permutation;
}
$isCorrect1 = function($p) //Test if a solution is correct
{
	$correct = true; //Assume solution is correct until proven incorrect.
	for($q=1;$q<=9;$q++)
	{
		if(implode(array_slice($p,0,$q)) % $q != 0) //If $q digits are NOT divisible by $q, declare incorrect.
		{
			$correct = false;
		}
	}
	return $correct;
};
$isCorrect2 = function($p)
{
	$correct = true; //Assume correct until proven otherwise
	$previousChange = -1; //Let 0 mean a decrease, 1 mean an increase.
	for($i = 1; $i < count($p); $i++)
	{
		$change = $p[$i] - $p[$i-1] > 0 ? 1 : 0;
		if($change == $previousChange)
		{
			$correct = false;
			break;
		}
		else
		{
			$previousChange = $change;
		}
	}
	return $correct;
};
$n = 0; //Number of tries
$n_p = 0; //Number of correct permutations
$q = str_split("123"); //Smallest possible permutation
do //Loop through all permutations
{
	$n++;
	if($isCorrect2($q))
	{
		$n_p++;
		echo implode($q)."\n";
	}
}
while($q = permute($q));
echo "$n_p solution(s) found after $n permutations.\n";
?>